namespace OpenAPI_Try.DataBase
{
    /// <summary>
    /// Класс выборки.
    /// </summary>
    public class Sample
    {
        /// <summary>
        /// Уникальный идентификатор выборки
        /// </summary>
        private int sampleId;
        /// <summary>
        /// ID элемента
        /// </summary>
        private int itemId;
        /// <summary>
        /// Значение.
        /// </summary>
        private int value;

        #region Свойства
        public int Id
        { get { return sampleId; } }

        public int ItemId
        {
            get { return itemId; }
            set { this.itemId = value; }
        }
        public int Value
        {
            get { return value; }
            set { this.value = value; }
        }
        #endregion

        /// <summary>
        /// Конструктор, который присваивает значения для readonly свойств.
        /// </summary>
        /// <param name="id">Уникальный ID выборки.</param>
        public Sample(int id)
        {
            sampleId = id;
            itemId = 0;
            value = 0;
        }
    }
}
