using System.Collections.Generic;
using System.Linq;

namespace OpenAPI_Try.DataBase
{
    /// <summary>
    /// Контейнер, содержащий все элементы из предоставляемой базы данных
    /// </summary>
    public static class ItemRegistry
    {
        /// <summary>
        /// Контейнер из элементов, которые хранятся по ID
        /// </summary>
        private static Dictionary<int, Item> items = new Dictionary<int, Item>();

        private static Source DBSource = new Source(200);
        /// <summary>
        /// Внутренний список элементов
        /// </summary>
        public static IList<Item> Items { get { return items.Values.ToList(); } }

        /// <summary>
        /// Создает элемент и присваивает ему уникальный ID и имя.
        /// </summary>
        /// <param name="id">Уникальный идентификатор</param>
        /// <param name="name">Имя элемента</param>
        /// <returns>Возвращает созданный или найденный элемент или null в случае если ID выходит за границы.</returns>
        public static Item MakeItem(int id, string name)
        {
            if (id < Source.MaxItemIndex)
            {
                Item item;
                if (!items.TryGetValue(id, out item))
                {
                    item = new Item(id, name, DBSource);
                    items.Add(id, item);
                }
                else
                { item.Name = name; }

                return item;
            }
            return null;
        }

        /// <summary>
        /// Получает или создает элемент. 
        /// Если элемент уже существует - получает ссылку на него.
        /// Если элемент не сущесвутет - создает его.
        /// </summary>
        /// <param name="id">Уникальный идентификатор</param>
        /// <returns>Возвращает запрашиваемый элемент или null если ID выходит за границы.</returns>
        public static Item GetItem(int id)
        {
            Item item = null;
            if (id < Source.MaxItemIndex)
            {
                items.TryGetValue(id, out item);
            }
            return item;
        }
    }
}
