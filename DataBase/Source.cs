namespace OpenAPI_Try.DataBase
{
    /// <summary>
    /// Data Source 
    /// </summary>
    public class Source
    {
        /// <summary>
        /// Максимальный ID элемента
        /// </summary>
        private static int maxItemIndex = 1000;
        /// <summary>
        /// Размер базы данных
        /// </summary>
        private int DBSize = maxItemIndex;
        /// <summary>
        /// Хранит список элементов базы данных по ID
        /// </summary>
        private int[] DBItems;

        private object lockObject;

        public static int MaxItemIndex { get { return maxItemIndex; } }

        public Source(uint updateInterval)
        {
            DBSize = MaxItemIndex;
            DBItems = new int[DBSize];

            lockObject = new object();
        }

        /// <summary>
        /// Считывает значение указанного идентификатора элемента. 
        /// </summary>
        /// <param name="offset">Идентификатор</param>
        /// <returns></returns>
        public int ReadItem(int offset)
        {
            lock (lockObject)
            { return DBItems[offset]; }
        }
        /// <summary>
        /// Связывает значение элемента с указанным идентификатором 
        /// </summary>
        /// <param name="offset">Идентификатор</param>
        /// <param name="value">Значение</param>
        public void WriteItem(int offset, int value)
        {
            lock (lockObject)
            { DBItems[offset] = value; }
        }
    }
}
