namespace OpenAPI_Try.DataBase
{
    /// <summary>
    /// Элемент в базе данных. 
    /// </summary>
     public class Item
    {   
        /// <summary>
        /// Уникальный идентификатор элемента
        /// </summary>
        private int itemId;
        /// <summary>
        /// Имя элемента
        /// </summary>
        private string itemName;
        /// <summary>
        /// Data Source которая предоставляет исходные данные для каждого элемента
        /// </summary>
        private Source dataSource;
        /// <summary>
        /// Значение элемента
        /// </summary>
        private int value;

        #region Свойства

        public int Id
        { get { return itemId; } }
        
        public string Name
        {
            get { return itemName; }
            set { itemName = value; }
        }
        
        public int Value
        {
            get { return value; }
            set { dataSource.WriteItem(itemId, value);
                this.value = value; }
        }

        public Item(int id, string name, Source source)
        {
            itemId = id;
            itemName = name;
            dataSource = source;
            value = 0;
        }
        #endregion
    }
}
