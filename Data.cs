using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;

using OpenAPI_Try.DataBase;

namespace OpenAPI_Try
{
    /// <summary>
    /// Идентифицирует модель сущности в качестве параметра базового класса DataService.
    /// </summary>
    public class TimeSeriesDataService : DataService<TimeSeriesEntities>
    {
        /// <summary>
        /// Вызывается единожды для инициализации политики доступа.
        /// </summary>
        /// <param name="config">Объект, описывающий конфигурацию сервиса</param>
        public static void InitializeService(DataServiceConfiguration config)
        {
            config.SetEntitySetAccessRule("*", EntitySetRights.AllRead | EntitySetRights.AllWrite);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V2;
        }
    }
    /// <summary>
    /// Модель сущностей OData.
    /// </summary>
    public partial class TimeSeriesEntities
    {
        /// <summary>
        /// Текущая выборка элементов.
        /// </summary>
        public IQueryable<Sample> Samples
        {
            get { return (new List<Sample>()).AsQueryable<Sample>(); }
        }
        /// <summary>
        /// Коллекция.
        /// </summary>
        public IQueryable<Item> Items
        {
            get
            {
                Console.WriteLine("Returning {0} Items", ItemRegistry.Items.Count);
                return (from item in ItemRegistry.Items
                        select new Item(item.Id, item.Name, item.Value)).AsQueryable<Item>();
            }
        }
    }
    /// <summary>
    /// Описание класса выборки элементов.
    /// </summary>
    [DataServiceEntity]
    [DataServiceKey("SampleId")]
    public class Sample
    {
        /// <summary>
        /// ID выборки.
        /// </summary>
        public int SampleId { get; set; }
        /// <summary>
        /// ID элементов выборки.
        /// </summary>
        public int ItemIdSampled { get; set; }
        /// <summary>
        /// Значение выборки.
        /// </summary>
        public int SampleValue { get; private set; }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="id">ID выборки</param>
        /// <param name="itemId">ID элемнета</param>
        /// <param name="v">Значение</param>
        public Sample(int id, int itemId, int v)
        {
            SampleId = id;
            ItemIdSampled = itemId;
            SampleValue = v;
        }
    }
    /// <summary>
    /// Описание класса элементов.
    /// </summary>
    [DataServiceKey("ItemId")]
    public class Item
    {
        private static int nextItemId = 1000; 
        /// <summary>
        /// ID элемента
        /// </summary>
        public int ItemId { get; set; }
        /// <summary>
        /// Имя элемента
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// Значение элемента
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// Конструктор, который используется при создании нового элемента.
        /// </summary>
        public Item()
        {
            ItemId = ++nextItemId;
            ItemName = "";
            Value = 0;
        }
        /// <summary>
        /// Конструктор, который используется при преобразовании данных к виду текущей модели.
        /// </summary>
        /// <param name="id">ID элемента</param>
        /// <param name="name">Имя элемента</param>
        /// <param name="value">Значение</param>
        public Item(int id, string name, int value)
        {
            ItemId = id;
            ItemName = name;
            Value = value;
        }
    }
}