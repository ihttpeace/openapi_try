using System;
using System.Data.Services;
using System.Web.Http;

namespace OpenAPI_Try
{
    /// <summary>
    /// OData сервис, представленный в виде коллекции, ее выборкой и подписками.
    /// </summary>
    class Program
    {
        private static DataServiceHost odataHost = null;

        /// <summary>
        /// URL адрес.
        /// </summary>
        private static string BaseAddress = "http://localhost:10001/TimeSeriesData.svc";
      
        /// <summary>
        /// Точка входа.
        /// </summary>
        /// <param name="args">Аргументы командной строки</param>
        static void Main(string[] args)
        {
            odataHost = new DataServiceHost(typeof(TimeSeriesDataService), new Uri[] { new Uri(BaseAddress) });
            odataHost.Open();
            Console.ReadKey();
            odataHost.Close();
        }
    }
}
